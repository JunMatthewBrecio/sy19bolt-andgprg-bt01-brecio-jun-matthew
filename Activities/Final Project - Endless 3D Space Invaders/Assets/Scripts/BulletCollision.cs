﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
//public class enemyStats
//{
//    public int maxHp = 10;
//    public int currentHp = 10;
//}

public class BulletCollision : MonoBehaviour
{
    public enemyStats stats;
    public ScoreCounter counter;
    public float damage;

    void Start()
    {
    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.CompareTag("Enemy"))
        {
            collision.transform.GetComponent<EnemyController>().stats.currentHp -= damage;
            Destroy(gameObject);
        }
    }

    void Update()
    {
        Destroy(gameObject, 5);
    }
}
