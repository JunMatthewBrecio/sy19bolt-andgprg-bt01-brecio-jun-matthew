﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyBulletDestroyer : MonoBehaviour
{
    void Start()
    {

    }

    void Update()
    {
        Destroy(gameObject, 5);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Player" || collision.gameObject.tag == "Limit")
        {
            Destroy(gameObject);
        }
    }
}
