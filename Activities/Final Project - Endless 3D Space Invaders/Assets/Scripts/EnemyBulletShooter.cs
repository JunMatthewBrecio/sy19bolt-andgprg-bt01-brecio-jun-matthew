﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletShooter : MonoBehaviour
{
    public float damage = 1;
    public Transform enemyNozzle;
    public GameObject bulletPrefab1;
    public float fireRate = Random.Range(2f, 5f);
   

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        fireRate -= Time.deltaTime;

        if (fireRate <= 0f)
        {
            Instantiate(bulletPrefab1, enemyNozzle.transform.position, transform.rotation);
            
            fireRate = Random.Range(2f, 5f);
        }
        
    }
}
