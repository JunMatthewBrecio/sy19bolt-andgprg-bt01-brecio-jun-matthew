﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

[System.Serializable]

public class playerStats
{
    public float maxHp = 5;
    public float currentHp = 5;
}

public class PlayerControl : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed;
    public Transform nozzle1;
    public GameObject bulletPrefab1;
    public GameObject other;
    public AudioClip audioClip;
    public AudioSource audioSource;
    public playerStats stats;
    public AudioClip crashSound;
    public AudioSource crash;
    

    // Start is called before the first frame update
    void Start()
    {
        //Screen.SetResolution(610, 1920, false);
        audioSource = GetComponent<AudioSource>();
        crash = GetComponent<AudioSource>();
    }

    

    // Update is called once per frame
    void Update()
    {
        //Bullet Shooter
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(bulletPrefab1, nozzle1.transform.position, transform.rotation);
            audioSource.PlayOneShot(audioClip);
        }

        //Move Left
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += new Vector3(-1, 0, 0) * Time.deltaTime * movementSpeed;
        }

        //Move Right
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += new Vector3(1, 0, 0) * Time.deltaTime * movementSpeed;
        }

        //Player Death
        if (stats.currentHp <= 0)
        {
            Destroy(this.gameObject);
            SceneManager.LoadScene("GameOver");
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("EnemyBullet"))
        {
            stats.currentHp--;
            crash.PlayOneShot(crashSound);
        }
    }
}


