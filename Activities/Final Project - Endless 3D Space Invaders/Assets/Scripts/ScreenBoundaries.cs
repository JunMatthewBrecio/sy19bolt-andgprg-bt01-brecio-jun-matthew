﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenBoundaries : MonoBehaviour
{
    private Vector2 screenLimit;

    // Start is called before the first frame update
    void Start()
    {
    screenLimit = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 viewPos = transform.position;
        viewPos.x = Mathf.Clamp(viewPos.x, screenLimit.x, screenLimit.x * -1);
        viewPos.y = Mathf.Clamp(viewPos.y, screenLimit.y, screenLimit.y * -1);
        transform.position = viewPos;
    }
}
