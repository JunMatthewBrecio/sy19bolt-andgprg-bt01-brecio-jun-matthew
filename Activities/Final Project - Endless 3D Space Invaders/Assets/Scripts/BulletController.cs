﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float bulletSpeed;
    public float damage;
    public Transform enemyNozzle;
    public GameObject bulletPrefab1;
    private float fireRate;
    public AudioClip enemyAudioClip;
    public AudioSource enemyAudioSource;

    void Start()
    {
        fireRate = Random.Range(2f, 5f);
        transform.GetComponent<Rigidbody>().velocity = transform.forward * bulletSpeed;
        enemyAudioSource = GetComponent<AudioSource>();
    }
    void Update()
    {
        
        fireRate -= Time.deltaTime;

        if (fireRate <= 0f)
        {
            Instantiate(bulletPrefab1, enemyNozzle.transform.position, transform.rotation);
            enemyAudioSource.PlayOneShot(enemyAudioClip);
            fireRate = Random.Range(2f, 5f);
        }
        Destroy(bulletPrefab1, 5);
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.transform.GetComponent<PlayerControl>().stats.currentHp -= damage;
            Destroy(bulletPrefab1);
        }
        else if(collision.gameObject.tag == "Limit")
        {
            Destroy(bulletPrefab1);
        }
    }
}