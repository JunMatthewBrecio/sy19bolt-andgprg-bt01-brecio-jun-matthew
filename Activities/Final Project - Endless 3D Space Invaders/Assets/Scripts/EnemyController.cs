﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class enemyStats
{
    public float maxHp;
    public float currentHp;
    public int enemyScoreValue;
}

public class EnemyController : MonoBehaviour
{
    public enemyStats stats;

    void Start()
    {
        stats.currentHp = stats.maxHp;
    }
    // Update is called once per frame
    void Update()
    {
        if (stats.currentHp <= 0)
        {
            ScoreCounter.scoreValue += stats.enemyScoreValue;
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Limit")
        {
            Destroy(gameObject);
        }
    }
}