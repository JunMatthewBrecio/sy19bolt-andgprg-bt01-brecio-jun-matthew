﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]

public class PlayerBulletCollision : MonoBehaviour
{
    public playerStats playerStats;

    void Start()
    {
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("EnemyBullet"))
        {
            playerStats.currentHp--;
        }
    }

    void Update()
    {
        //if (stats.currentHp == 0)
        //{
        //    Destroy(this.gameObject);
        //}
    }
}