﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaController : MonoBehaviour
{
    public GameObject[] enemy = new GameObject[5];
    public int enemyNum;
    public int[] randomEnemy;
    public Vector3 enemyRange;
    public float[] enemySpeed;
    public int seed;
    private GameObject[] enemyClones;

    

    void Start()
    {
        StartCoroutine(enemyWave());
    }
    void spawnEnemy()
    {
        randomEnemy = new int[enemyNum];
        enemySpeed = new float[enemyNum];
        enemyClones = new GameObject[enemyNum];
        // Random.InitState(seed);
        for (int i = 0; i < enemyNum; i++)
        {
            randomEnemy[i] = Random.Range(0, 4);
            enemySpeed[i] = Random.Range(1, 3);
            enemyClones[i] = Instantiate(enemy[randomEnemy[i]], new Vector3(transform.position.x + Random.Range(-enemyRange.x, enemyRange.x),
                                                                                      transform.position.y + Random.Range(-enemyRange.y, enemyRange.y),
                                                                                      transform.position.z + Random.Range(-enemyRange.z, enemyRange.z)), Quaternion.identity);
            enemyClones[i].transform.GetComponent<Rigidbody>().velocity = transform.forward * enemySpeed[i];
            //  enemyClones[i].transform.parent = this.transform;
        }
    }
    IEnumerator enemyWave()
    {
        while (true)
        {
            yield return new WaitForSeconds(5);
            spawnEnemy();
        }
    }
}