﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour
{
    public static int scoreValue = 0;
    public Text score;
    public EnemyStats currentHp;

    void Start()
    {
        score = GetComponent<Text>();
    }


    void Update()
    {
        if (GetComponent<BulletCollision>().stats.currentHp == 0)
        {
            score.text = "You win!";
        }
        //score.text = "Score: " + scoreValue;
    }
}
