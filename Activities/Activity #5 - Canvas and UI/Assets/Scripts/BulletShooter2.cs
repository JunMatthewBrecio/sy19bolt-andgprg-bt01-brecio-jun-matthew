﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooter2 : MonoBehaviour
{
    public Transform nozzle2;

    public GameObject bulletPrefab2;

    public GameObject other;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            Instantiate(bulletPrefab2, nozzle2.transform.position, transform.rotation);

            Destroy(other, 5f);
        }
    }
}
