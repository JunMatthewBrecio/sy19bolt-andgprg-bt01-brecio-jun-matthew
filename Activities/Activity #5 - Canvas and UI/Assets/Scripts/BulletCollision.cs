﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class enemyStats
{
    public int maxHp = 10;
    public int currentHp = 10;
}

public class BulletCollision : MonoBehaviour
{
    public enemyStats stats;
    public ScoreCounter counter;

    void Start()
    {
    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.CompareTag("Bullet"))
        {
            stats.currentHp--;
        }
    }

    void Update()
    {
        if (stats.currentHp <= 0)
        {
            Destroy(this.gameObject);
            //counter.score.text = "You win!";
            //ScoreCounter.scoreValue += 1;
        }
    }
}
