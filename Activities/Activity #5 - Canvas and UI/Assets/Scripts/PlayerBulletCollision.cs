﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class tankerStats
{
    public int maxHp = 10;
    public int currentHp = 10;
}

public class PlayerBulletCollision : MonoBehaviour
{
    public tankerStats stats;

    void Start()
    {
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wood"))
        {
            stats.currentHp--;
        }
    }

    void Update()
    {
        if (stats.currentHp == 0)
        {
            Destroy(this.gameObject);
        }
    }
}