﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlideController : MonoBehaviour
{
    public BulletCollision enemy;
    public Slider hp;
    public Image colorChange;

    public Color[] fillColor = new Color[3];

    void Start()
    {
        hp.wholeNumbers = true;
        hp.minValue = 0;

        fillColor[0] = new Color(0, 255, 0); //green
        fillColor[1] = new Color(254, 161, 0); //orange
        fillColor[2] = new Color(255, 0, 0); //red
    }


    void Update()
    {
        hp.maxValue = enemy.stats.maxHp;
        hp.value = enemy.stats.currentHp;

        colorChange.color = fillColor[0];
        if (hp.value <= enemy.stats.maxHp / 2) colorChange.color = fillColor[1];
        if (hp.value <= enemy.stats.maxHp / 3 && hp.value > enemy.stats.maxHp / 2) colorChange.color = fillColor[1];
        if (hp.value <= enemy.stats.maxHp / 10) colorChange.color = fillColor[2];
    }
}
