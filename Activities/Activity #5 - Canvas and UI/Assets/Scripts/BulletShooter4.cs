﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooter4 : MonoBehaviour
{
    public Transform nozzle4;

    public GameObject bulletPrefab4;

    public GameObject other;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Instantiate(bulletPrefab4, nozzle4.transform.position, transform.rotation);

            Destroy(other, 5f);
        }
    }
}
