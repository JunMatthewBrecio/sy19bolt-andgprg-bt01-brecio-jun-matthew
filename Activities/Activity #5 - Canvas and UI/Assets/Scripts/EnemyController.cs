﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class EnemyStats
{
    public float maxHp;
    public float currentHp;
    //public float damage;

}

public class EnemyController : MonoBehaviour
{
    public EnemyStats stats;

    // Start is called before the first frame update
    void Start()
    {
        stats.currentHp = stats.maxHp;
    }

    // Update is called once per frame
    void Update()
    {
        if (stats.currentHp <= 0)
        {
            ScoreCounter.scoreValue += 1;
            Destroy(gameObject);
        }
    }
}
