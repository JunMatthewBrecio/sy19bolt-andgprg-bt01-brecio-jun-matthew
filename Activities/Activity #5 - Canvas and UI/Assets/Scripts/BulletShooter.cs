﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class BulletShooter : MonoBehaviour
{
    public Transform nozzle1;
    public GameObject bulletPrefab1;
    public GameObject other;
    public AudioClip audioClip;
    public AudioSource audioSource;
    public int bulletCount = 30;

    public Text bulletCountUI;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        bulletCountUI.text = "Bullet Count: " + bulletCount;
    }

    // Update is called once per frame
    void Update()
    {
        if (bulletCount > 0)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                Instantiate(bulletPrefab1, nozzle1.transform.position, transform.rotation);
                audioSource.PlayOneShot(audioClip);
                bulletCount--;
                bulletCountUI.text = "Bullet Count: " + bulletCount;
            }

        }
        else
        {
            bulletCountUI.text = "Bullet Count: Empty";
            //GetComponent<Text>().color = Color.red;
            //bulletCountUI.text = Color.red;
            if (Input.GetKeyDown(KeyCode.R))
            {
                bulletCount = 30;
                bulletCountUI.text = "Reloading..";
                bulletCountUI.text = "Bullet Count: " + bulletCount;
            }
        }
    }

    
}