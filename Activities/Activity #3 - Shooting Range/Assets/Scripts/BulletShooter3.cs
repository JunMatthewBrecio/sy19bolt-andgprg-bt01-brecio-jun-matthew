﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooter3 : MonoBehaviour
{
    public Transform nozzle3;

    public GameObject bulletPrefab3;

    public GameObject other;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Instantiate(bulletPrefab3, nozzle3.transform.position, transform.rotation);

            Destroy(other, 5f);
        }
    }
}
