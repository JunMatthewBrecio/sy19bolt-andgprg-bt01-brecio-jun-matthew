﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooter : MonoBehaviour
{
    public Transform nozzle1;

    public GameObject bulletPrefab1;

    public GameObject other;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Instantiate(bulletPrefab1, nozzle1.transform.position, transform.rotation);
        }
        //Destroy(bulletPrefab1, 5f);
    }
}
