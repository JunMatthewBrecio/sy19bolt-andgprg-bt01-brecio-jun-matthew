﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollision : MonoBehaviour
{
    public Transform Spawnpoint;
    public GameObject Prefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(this.gameObject);
            ScoreCounter.scoreValue += 1;
        }
    }

    

    // Update is called once per frame
    void Update()
    {
        
    }
}
