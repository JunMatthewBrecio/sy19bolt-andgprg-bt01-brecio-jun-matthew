﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alert : MonoBehaviour
{
    public Transform Spawnpoint;
    public GameObject Prefab;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == ("Player"))
        {
            Debug.Log("Alert");
            Instantiate(Prefab, Spawnpoint.position, Spawnpoint.rotation);
            //Destroy(gameObject, 1);
        }
    }

    void update()
    {
        
    }
}
