﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImprovedPlayerMovement : MonoBehaviour
{
    public float speed = 1;
    public float rotationSpeed = 10.0f;

    // Update is called once per frame
    void Update()
    {
        float translation = Input.GetAxis("Vertical") * speed;
    float rotation = Input.GetAxis("Horizontal") * rotationSpeed;

        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;

        transform.Translate(0, 0, translation);

        transform.Rotate(0, rotation, 0);
    }
}
