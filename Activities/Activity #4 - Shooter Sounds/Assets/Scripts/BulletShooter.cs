﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooter : MonoBehaviour
{
    public Transform nozzle1;
    public GameObject bulletPrefab1;
    public GameObject other;
    public AudioClip audioClip;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Instantiate(bulletPrefab1, nozzle1.transform.position, transform.rotation);
            audioSource.PlayOneShot(audioClip);
        }
    }
}
